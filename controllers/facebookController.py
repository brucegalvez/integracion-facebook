from settings import Environment
import json
import time
import requests
from utils import Utils
from flask import request


class FacebookController(Environment):

    def __init__(self):
        self.utils = Utils()
        envFacebook = self.facebook()
        self.TOKEN = envFacebook['TOKEN']
        self.ACCESS_TOKEN = envFacebook['ACCESS_TOKEN']
        self.URI_API_GRAPH = envFacebook['URI_API_GRAPH']
        self.order = ['countries', 'leagues', 'teams', 'players', 'player']

    def verify(self):
        if(request.args.get('hub.verify_token', '') == self.TOKEN):
            print('verified')
            return request.args.get('hub.challenge', ''), 200
        else:
            print('Wrong token')
            return 'Error, wrong validation token', 403

    def webhook(self):
        data = request.get_json()
        senderId = ''
        if data["object"] == "page":
            for entry in data["entry"]:
                webhookEvent = entry["messaging"][0]
                senderId = webhookEvent["sender"]["id"]
                self.sendAction(senderId, "mark_seen")
                self.sendAction(senderId, "typing_on")
                if webhookEvent.get("message"):
                    self.handleMessage(senderId, webhookEvent["message"])
                elif webhookEvent.get("postback"):
                    self.handlePostback(senderId, webhookEvent["postback"])
                self.sendAction(senderId, "typing_off")
            return "ok", 200
        else:
            return "404 not found", 404

    def handleMessage(self, senderId, receivedMessage):
        if receivedMessage['text']:
            if receivedMessage['text'] != 'football':
                self.sendTextMessage(senderId, 'Escribe football para empezar')
            else:
                buttons = self.makeButtons([
                    {'name': 'peru'},
                    {'name': 'argentina'},
                    {'name': 'brasil'}
                ], 'countries')
                self.sendPostbackButtons(senderId, buttons)
        elif receivedMessage['attachments']:
            self.sendPostbackButtons(senderId, receivedMessage)

    def handlePostback(self, senderId, receivedMessage):
        payloadField = 'countries'
        query = receivedMessage['payload']
        if '-' in query:
            payloadField, query = query.split('-')
        field = self.order[self.order.index(payloadField) + 1]
        results = self.utils.searchApiFootball(field, query)
        buttons = self.makeButtons(results, field)
        self.sendPostbackButtons(senderId, buttons[:3])

    def makeButtons(self, options, field):
        buttons = []
        field_id = 'name'
        if field != 'countries':
            field_id = 'id'
        for option in options:
            buttons.append({
                "type": "postback",
                "title": option['name'],
                "payload": field+'-'+str(option[field_id])
            })
        return buttons

    def sendTextMessage(self, senderId, receivedMessage):
        response = {'message': {"text": receivedMessage}}
        self.callSendAPI(senderId, response)

    def sendPostbackButtons(self, senderId, receivedButtons):
        response = {'message': {"attachment": {
            "type": "template",
            "payload": {
                "template_type": "button",
                "text": "Escoje una opcion:",
                "buttons": receivedButtons
            }
        }}}
        self.callSendAPI(senderId, response)

    def sendAction(self, senderId, action):
        response = {"sender_action": action}
        self.callSendAPI(senderId, response)

    def callSendAPI(self, recipientId, response):
        params = {"access_token": self.ACCESS_TOKEN}
        headers = {"Content-Type": "application/json"}
        data = {"recipient": {"id": recipientId}}
        url = "".join([self.URI_API_GRAPH, "/me/messages"])
        data.update(response)
        data = json.dumps(data)
        respuesta = requests.post(
            url, data=data, params=params, headers=headers)
        if respuesta.status_code != 200:
            print(respuesta.text)
