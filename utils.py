import requests
import json
from settings import Environment


class Utils(Environment):

    def __init__(self):
        envFootball = self.football()
        self.URL_API_FOOTBALL = envFootball["URL_API_FOOTBALL"]
        self.headers = {
            'x-rapidapi-host': "api-football-v1.p.rapidapi.com",
            'x-rapidapi-key': "7b77796e5fmsha66e885da59c380p16c3fbjsn38c876bd0705"
        }

    def searchApiFootball(self, field, query):
        addedUrls = {
            'leagues': '/leagues/country/'+query+'/2019',
            'teams': '/teams/league/'+query,
            'players': '/players/squad/'+query,
            'player': '/players/player/'+query
        }
        url = self.URL_API_FOOTBALL + addedUrls[field]
        data = requests.request("GET", url, headers=self.headers).json()
        objectsNames = []
        if field == 'player':
            player = data['api']['players'][0]
            objectsNames.append(player)
        else:
            objects = data['api'][field]
            sField = field[:-1]
            field_id = sField + "_id"
            for element in objects:
                objectsNames.append({
                    'name': element['name'],
                    'id': element[field_id]
                })
        return objectsNames
