from flask import Blueprint
from controllers.facebookController import FacebookController
facebookController = FacebookController()

facebookBP = Blueprint('facebookRoutes', __name__)


@facebookBP.route('/', methods=['GET'])
def verify():
    return facebookController.verify()


@facebookBP.route('/', methods=['POST'])
def webhook():
    return facebookController.webhook()
