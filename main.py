import requests
from flask import Flask, Blueprint
from routes.facebookRoutes import facebookBP


def create_app():
    app = Flask(__name__)
    app.register_blueprint(facebookBP)
    return app


if __name__ == '__main__':
    app = create_app()
    app.run(port=8000, debug=True)
